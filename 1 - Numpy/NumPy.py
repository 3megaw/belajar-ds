my_list = [1,2,3,4]
import numpy as np

arr = np.array(my_list)
print(arr)

my_mat = [[1,2,3],[4,5,6],[7,8,9]]
print(my_mat)
arr2 = np.array(my_mat)
print(arr2)

arr3 = np.arange(0,11)
print(arr3)
arr4 = arr3 + arr3
print(arr4)
